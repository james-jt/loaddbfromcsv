﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Oracle.ManagedDataAccess.Client;
using System.Windows.Forms;
using PinsLoader.Model;

namespace PinsLoader.DataAccess
{
    public class DataAccessClass
    {
        string connectionString;
        SqlConnection connection = null;
        //OracleConnection connection = null;
        private string sqlCommand = string.Empty;
        SqlCommand command;
        //OracleCommand command;

        public DataAccessClass()
        {
            connectionString = ConfigurationManager.ConnectionStrings["CrsConnection"].ConnectionString;
            var testConnection = new SqlConnection(connectionString);
            //var testConnection = new OracleConnection(connectionString);
            try
            {
                testConnection.Open();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return;
            }
            connection = testConnection;
            command = connection.CreateCommand();
            command.CommandType = CommandType.Text;
        }

        private DataTable ExecuteDatabaseQuery(string query)
        {
            if (connection == null)
            {
                return null;
            }
            try
            {
                command.CommandText = query;
                SqlDataAdapter da = new SqlDataAdapter(command);
                //OracleDataAdapter da = new OracleDataAdapter(command);
                DataTable resultTable = new DataTable();
                da.Fill(resultTable);
                return resultTable;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public bool InsertPins(PinsTable pinsTable)
        {
            if (connection == null)
            {
                return false;
            }

            for (int i = 0; i < pinsTable.Rows.Count; i++)
            {
                DataRow dr = pinsTable.Rows[i];
                sqlCommand = String.Format("SELECT DENOMINATION FROM AIRTIME_ALL WHERE SERIAL_NO = '{0}'", dr[0].ToString());
                DataTable dt = ExecuteDatabaseQuery(sqlCommand);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //Record already exists, skip gracefully. 
                    continue;
                }

                sqlCommand = string.Format("INSERT INTO AIRTIME_ALL VALUES ('{0}', '{1}',  '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}')",
                    dr[0].ToString(),
                    dr[1].ToString(),
                    dr[2].ToString(),
                    dr[3].ToString(),
                    dr[4].ToString(),
                    dr[5].ToString(),
                    dr[6].ToString(),
                    dr[7].ToString(),
                    dr[8].ToString(),
                    dr[9].ToString()
                    );

                command.CommandText = sqlCommand;
                try
                {
                    command.ExecuteNonQuery();
                    //Make log entry
                }
                catch (Exception e)
                {
                    //Make log entry
                    MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            return true;
        }
    }
}
