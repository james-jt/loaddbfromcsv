﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PinsLoader.BusinessLogic;
using PinsLoader.Model;

namespace PinsLoader
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            pinsTable = new PinsTable();
            FormatDataGridView();
        }

        public string InputFilePath { get; set; }
        public PinsTable pinsTable { get; set; }

        private void FormatDataGridView()
        {
            this.dataGridView.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font(this.dataGridView.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            this.dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToOrderColumns = false;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.EditMode = DataGridViewEditMode.EditProgrammatically;
            this.dataGridView.BackgroundColor = Color.GhostWhite;
            this.dataGridView.BorderStyle = BorderStyle.None;
            if (dataGridView.Rows.Count > 0)
            {
                this.dataGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
                this.dataGridView.Columns[6].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cmbDenomination.Text = string.Empty;
            cmbProduct.Text = string.Empty;
            txtInputFilePath.Text = string.Empty;
            pinsTable.Clear();
            dataGridView.DataSource = null;
            lblStatus.Text = "Ready...";
        }
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            var fileOpenDialogue = new OpenFileDialog();
            fileOpenDialogue.Filter = "Text File (*.txt)|*.txt|All Files  (*.*)|*.*";
            fileOpenDialogue.ShowDialog();
            if (string.IsNullOrEmpty(fileOpenDialogue.FileName))
            {
                return;
            }
            if (!(fileOpenDialogue.FileName.EndsWith(".txt")))
            {
                MessageBox.Show("Invalid file type selected.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            txtInputFilePath.Text = fileOpenDialogue.FileName.ToString();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            pinsTable.Clear();
            InputFilePath = txtInputFilePath.Text;
            if (string.IsNullOrEmpty(cmbDenomination.Text))
            {
                MessageBox.Show("Please enter or select a denomination.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmbDenomination.Focus();
                return;
            }
            float denomination;
            if(!(float.TryParse(cmbDenomination.Text, out denomination)))
            {
                MessageBox.Show("Please enter a valid denomination.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmbDenomination.Focus();
                return;
            }

            if (string.IsNullOrEmpty(cmbProduct.Text))
            {
                MessageBox.Show("Please enter or select a product.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmbProduct.Focus();
                return;
            }

            if (string.IsNullOrEmpty(InputFilePath))
            {
                MessageBox.Show("Please enter a valid file path for the input file.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtInputFilePath.Focus();
                return;
            }
            if (!(InputFilePath.EndsWith(".txt")))
            {
                MessageBox.Show("Please enter a valid file type for the input file.", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtInputFilePath.Focus();
                return;
            }

            lblStatus.Text = "Importing from text file...";
            Application.DoEvents();
            pinsTable = new BusinessLogicClass().ReadFromTextFile(InputFilePath, denomination, pinsTable);
            dataGridView.DataSource = pinsTable;
            FormatDataGridView();
            lblStatus.Text = "Ready to upload pins to database.";
            Application.DoEvents();
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            if(pinsTable.Rows.Count < 1)
            {
                MessageBox.Show("You must first import pins from an input text file.", "Missing input", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                btnImport.Focus();
                return;
            }
            if (MessageBox.Show($"Are you sure you want to upload ${cmbDenomination.Text} denominated {cmbProduct.Text} pins from \"{InputFilePath}\" into the database?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                lblStatus.Text = "Processing...";
                Application.DoEvents();
                if (new BusinessLogicClass().UploadPins(pinsTable))
                {
                    lblStatus.Text = "Upload complete.";
                    Application.DoEvents();
                    MessageBox.Show("Pins successfully uploaded to database.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    lblStatus.Text = "Error encountered.";
                    Application.DoEvents();
                    MessageBox.Show("Failed to uploaded pins to database.", "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                lblStatus.Text = "Operation aborted by user.";
            }
        }

        private void cmbDenomination_SelectedIndexChanged(object sender, EventArgs e)
        {
            int check;
            if (!(int.TryParse(cmbDenomination.Text, out check)))
            {
                MessageBox.Show("Enter a valid denomination", "Invalid input", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cmbDenomination.SelectAll();
                cmbDenomination.Focus();
            }
        }
    }
}
