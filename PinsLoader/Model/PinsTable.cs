﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace PinsLoader.Model
{
    public class PinsTable : DataTable
    {
        public PinsTable()
            : base()
        {
            this.Columns.Add("SERIAL_NO");
            this.Columns.Add("SCRATCH_NO");
            this.Columns.Add("DENOMINATION");
            this.Columns.Add("DATE_RECEIVED");
            this.Columns.Add("DATE_DISTRIBUTED");
            this.Columns.Add("W_HOUSE");
            this.Columns.Add("DISTRIBUTED");
            this.Columns.Add("ACTIVATED");
            this.Columns.Add("USERS");
            this.Columns.Add("COMMENTS");
        }
    }
}
