﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using PinsLoader.DataAccess;
using PinsLoader.Model;

namespace PinsLoader.BusinessLogic
{
    public class BusinessLogicClass
    {
        public bool UploadPins(PinsTable pinsTable)
        {
            return new DataAccessClass().InsertPins(pinsTable);
        }

        public PinsTable ReadFromTextFile(string path, float denomination, PinsTable pinsTable)
        {
            var lines = File.ReadLines(path);
            lines = lines.Skip(1).Where(l => l != string.Empty);
            for(int i = 0; i < lines.Count(); i++)
            {
                var line = lines.ToArray()[i];
                var splitLine = line.Split(',');
                if(line.Contains(" ") || splitLine.Length != 2)
                {
                    //Invalid input file.
                    System.Windows.Forms.MessageBox.Show($"Invalid data encountered at line {i + 1} of the input text file.", "Invalid Data", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                    pinsTable.Clear();
                    break;
                }
                DataRow dr = pinsTable.NewRow();
                dr["SERIAL_NO"] = splitLine[0];
                dr["SCRATCH_NO"] = splitLine[1];
                dr["DENOMINATION"] = denomination;
                dr["DATE_RECEIVED"] = DateTime.Now;
                dr["DATE_DISTRIBUTED"] = DBNull.Value;
                dr["W_HOUSE"] = DBNull.Value;
                dr["DISTRIBUTED"] = "no";
                dr["ACTIVATED"] = DBNull.Value;
                dr["USERS"] = DBNull.Value;
                dr["COMMENTS"] = DBNull.Value;
                pinsTable.Rows.Add(dr);
            }
            return pinsTable;
        }
    }
}
